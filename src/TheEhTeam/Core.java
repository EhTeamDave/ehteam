package TheEhTeam;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.table.TableModel;

public class Core {

	private static ConnectionManager cM_;
	
	public Core() {
		initCore();
	}
	
	private void initCore() {
		String host = "jdbc:mysql://5.135.86.36:3306/dcoding1_ehteam?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		String user = "dcoding1_ehteam";
		String password = "p@Z{ELQt5s";
		
		cM_ = new ConnectionManager(host, user, password);
		cM_.setDebugger_(true); 				//DEFAULT FALSE
		cM_.setPrintErrorsByJOption_(true); 	//DEFAULT FALSE
	}
	
	public static void LogingPopup(JFrame previousFrame) {
		previousFrame.dispose();
		new LoginPanel();
	}
	
	public static void CreateUserPopup(JFrame previousFrame) {
		previousFrame.dispose();
		new CreateUser();
	}
	
	public static void MainGUIPopup(JFrame previousFrame) {
		previousFrame.dispose();
		new GUI();
	}
	
	public static TableModel peformQuery(String query) {
		cM_.initiateConnection();
		TableModel model = cM_.dumpResultSetToTableModel(cM_.executeQuery(query));
		cM_.closeConnection();
		return model;
	}
	
	public static boolean Login(String userName, char [] password) throws SQLException {
		cM_.initiateConnection();
		
		ResultSet results = cM_.executeQuery("SELECT * FROM dcoding1_ehteam.users "
											+ "WHERE name='" + userName + "' "
											+ "AND password='" + new String(password) + "';");
		boolean result = results.next();
		
		cM_.closeConnection();
		return result;
	}
	
	public static boolean CreateUser(String update) {
		cM_.initiateConnection();
		
		int results = cM_.executeUpdate(update);
		cM_.closeConnection();
		if (results == -1) return false;
		return true;
	}
}
