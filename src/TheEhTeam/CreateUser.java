package TheEhTeam;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class CreateUser {

	//--GUI
	private JFrame frame_;
	private JPanel  cContainer_, sContainer_;
	private JLabel userLbl_, passwordLbl_, roleLbl;
	private JTextField userField_;
	private JPasswordField passwordField_;
	private String [] roles_ = {"PMO", "Team Member"};
	@SuppressWarnings("rawtypes")
	private JComboBox roleBox_ ;
	private JButton submit_, clear_;
	
	//--Core
	private ButtonHandler btn_;

	public CreateUser() {
		initializeCore();
		setFrame();
		buildCenterContainer();
		buildSouthContainer();
		finalizeFrame();
	}

	private void initializeCore() {
		btn_ = new ButtonHandler();
	}
	
	private void setFrame() {
		frame_ = new JFrame("Register");
		frame_.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_.setSize(400, 300);
		frame_.setResizable(false);
		frame_.setLocationRelativeTo(null);//Centers frame on screen
		frame_.setLayout(new BorderLayout());//Sets the layout manager
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })	//roleBox_
	private void buildCenterContainer() {
		//--Initialize
		cContainer_ = new JPanel();
			userLbl_ = new JLabel("Username: ");
			userField_ = new JTextField(10);
			passwordLbl_ = new JLabel("Password: ");
			passwordField_ = new JPasswordField(10);
			roleLbl = new JLabel("Role: ");
			roleBox_ = new JComboBox(roles_);
			
		//--Configure
		cContainer_.setLayout(new GridLayout(3,2));
		
		//--Add
		cContainer_.add(userLbl_);
		cContainer_.add(userField_);
		cContainer_.add(passwordLbl_);
		cContainer_.add(passwordField_);
		cContainer_.add(roleLbl);
		cContainer_.add(roleBox_);
	}
	
	private void buildSouthContainer() {
		//--Initialize
		sContainer_ = new JPanel();
			submit_ = new JButton("Submit");
			clear_ = new JButton("Clear");
		
		//--Configure
		sContainer_.setLayout(new FlowLayout());
			submit_.addActionListener(btn_);
			clear_.addActionListener(btn_);
		
		//--Add
		sContainer_.add(submit_);
		sContainer_.add(clear_);
	}
	
	private void finalizeFrame() {
		//frame_.setJMenuBar(mB_);
		//frame_.add(wContainer_, BorderLayout.WEST);
		frame_.add(cContainer_, BorderLayout.CENTER);
		frame_.add(sContainer_, BorderLayout.SOUTH);
		frame_.pack();		//Auto-Sizes Frame Accordingly
		frame_.setVisible(true);
	}
	
	private class ButtonHandler implements ActionListener{	
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(submit_)) {
				if (Submit()) Core.LogingPopup(frame_);
			}	//replace null with button
			if (e.getSource().equals(clear_)) {
				Clear();
			}	//replace null with button
		}
	}
	
	//We need to Code this
	public boolean Submit() {
		if (Core.CreateUser("INSERT INTO users (name, password, roles_id) "
							+ "VALUES ('" + userField_.getText() +"','" 
							+ new String(passwordField_.getPassword()) + "',"
							+ roleBox_.getSelectedIndex() + 1 + ");")) {//Add 1 to the Selected ArrayItem, ID's dont start at 0	
			return true;
		}
		JOptionPane.showMessageDialog(null, "The user could not be found, or the user does not exist.\n"
				+ "Please try again, or create a new user.");
		return false;
	}
	
	public void Clear() {
		userField_.setText("");
		passwordField_.setText("");
	}
}
