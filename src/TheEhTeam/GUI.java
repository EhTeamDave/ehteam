package TheEhTeam;

import javax.swing.*;
import java.awt.Dialog;
import java.awt.*;
import java.awt.event.*;

public class GUI {

	//--GUI
	private JFrame frame_;
	private JPanel wContainer_, cContainer_, sContainer_;
	private JLabel  patchLbl_, messageLbl_;
	private JTable table_;
	private JButton addProjectBtn_, addTaskBtn_;
	private AddAProjectPanel addProjPanel;
	private AddATaskPanel addTaskPanel;
	
	//--Core
	private ButtonHandler btn_;

	public GUI() {
		initializeCore();
		setFrame();
		buildWestContainer();
		buildCenterContainer();
		buildSouthContainer();
		finalizeFrame();
	}

	private void initializeCore() {
		btn_ = new ButtonHandler();
	}
	
	private void setFrame() {
		frame_ = new JFrame("Product Backlog Manager");
		frame_.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_.setSize(800, 300);
		frame_.setLocationRelativeTo(null);//Centers frame on screen
		frame_.setLayout(new BorderLayout());//Sets the layout manager
	}
	
	private void buildWestContainer() {
		//--Initialize
		wContainer_ = new JPanel();
		
		//--Configure
		wContainer_.setLayout(new GridLayout(6, 1));
		
		//--Add
		wContainer_.add(new JButton("We"));
		wContainer_.add(new JButton("Can"));
		wContainer_.add(new JButton("Add"));
		wContainer_.add(new JButton("Additional"));
		wContainer_.add(new JButton("Buttons"));
		wContainer_.add(new JButton("Here"));

		
	}
	
	private void buildCenterContainer() {
		
		addProjectBtn_ = new JButton("Add a new Project");
		addTaskBtn_ = new JButton("Add a new Task");
		
		//--Initialize - Parent/Child Structure 
		cContainer_ = new JPanel();
			JPanel cCMainPanel = new JPanel();
				table_ = new JTable(Core.peformQuery("SELECT * FROM task;"));
				JScrollPane scroll = new JScrollPane(table_);	
			JPanel cCSouthPanel = new JPanel();
		
		//--Configure
		cContainer_.setLayout(new BorderLayout());
			cCMainPanel.setLayout(new BorderLayout());
			cCSouthPanel.setLayout(new FlowLayout());
		
		//--Add Components
		cContainer_.add(cCMainPanel, BorderLayout.CENTER);
			cCMainPanel.add(scroll, BorderLayout.CENTER);
		cContainer_.add(cCSouthPanel, BorderLayout.SOUTH);
		
		addProjectBtn_.addActionListener(btn_);
		addTaskBtn_.addActionListener(btn_);
		
			cCSouthPanel.add(addProjectBtn_);
			cCSouthPanel.add(addTaskBtn_);
	}
	
	private void buildSouthContainer() {
		//--Initialize
		sContainer_ = new JPanel();
			patchLbl_ = new JLabel("Version: PRE-ALPHA");
			messageLbl_ = new JLabel("Now Viewing All Tasks");
		
		//--Configure
		sContainer_.setLayout(new BorderLayout());
		
		//--Add
		sContainer_.add(patchLbl_, BorderLayout.WEST);
		sContainer_.add(messageLbl_, BorderLayout.EAST);
	}
	
	private void finalizeFrame() {
		//frame_.setJMenuBar(mB_);
		frame_.add(wContainer_, BorderLayout.WEST);
		frame_.add(cContainer_, BorderLayout.CENTER);
		frame_.add(sContainer_, BorderLayout.SOUTH);
		//frame_.pack();		//Auto-Sizes Frame Accordingly
		frame_.setVisible(true);
	}
	
	private class ButtonHandler implements ActionListener{	
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(null)) {}	//replace null with button
			if (e.getSource().equals(addProjectBtn_)) {
				addProjPanel= new AddAProjectPanel();
				JFrame frame = new JFrame("Add a New Project");
	      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	      frame.setSize(600, 500);      
	      frame.setLocationRelativeTo(null); 
	      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	      frame.setVisible(true);
	      
	      frame.getContentPane().add(addProjPanel, BorderLayout.CENTER);

			}
			if (e.getSource().equals(addTaskBtn_)) {
				addTaskPanel= new AddATaskPanel();
				JFrame frame = new JFrame("Add a New Task");
	      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	      frame.setSize(600, 500);      
	      frame.setLocationRelativeTo(null); 
	      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	      frame.setVisible(true);
	      
	      frame.getContentPane().add(addTaskPanel, BorderLayout.CENTER);

			}
		}
	}
}
