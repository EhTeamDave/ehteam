package TheEhTeam;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginPanel {

	//--GUI
		private JFrame frame_;// = new JFrame("Login");
		private JPanel  cContainer_, sContainer_;
		private JLabel userLbl_, passwordLbl_;
		private JTextField userField_;
		private JPasswordField passwordField_;
		private JButton signInBtn_, signUpBtn_;
		
		//--Core
		private ButtonHandler btn_;

		public LoginPanel() {
			initializeCore();
			setFrame();
			buildCenterContainer();
			buildSouthContainer();
			finalizeFrame();
		}

		private void initializeCore() {
			btn_ = new ButtonHandler();
		}
		
		private void setFrame() {
			frame_ = new JFrame("Login");
			frame_.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame_.setSize(400, 300);
			frame_.setResizable(false);
			frame_.setLocationRelativeTo(null);//Centers frame on screen
			frame_.setLayout(new BorderLayout());//Sets the layout manager
		}
		
		private void buildCenterContainer() {
			//--Initialize
			cContainer_ = new JPanel();
				userLbl_ = new JLabel("Username: ");
				userField_ = new JTextField(10);
				passwordLbl_ = new JLabel("Password: ");
				passwordField_ = new JPasswordField(10);
				
			//--Configure
			cContainer_.setLayout(new GridLayout(2,2));
			
			//--Add
			cContainer_.add(userLbl_);
			cContainer_.add(userField_);
			cContainer_.add(passwordLbl_);
			cContainer_.add(passwordField_);
		}
		
		private void buildSouthContainer() {
			//--Initialize
			sContainer_ = new JPanel();
				signInBtn_ = new JButton("Sign In");
				signUpBtn_ = new JButton("Register");
			
			//--Configure
			sContainer_.setLayout(new FlowLayout());
				signInBtn_.addActionListener(btn_);
				signUpBtn_.addActionListener(btn_);
			
			//--Add
			sContainer_.add(signInBtn_);
			sContainer_.add(signUpBtn_);
		}
		
		private void finalizeFrame() {
			//frame_.setJMenuBar(mB_);
			//frame_.add(wContainer_, BorderLayout.WEST);
			frame_.add(cContainer_, BorderLayout.CENTER);
			frame_.add(sContainer_, BorderLayout.SOUTH);
			frame_.pack();		//Auto-Sizes Frame Accordingly
			frame_.setVisible(true);
		}
		
		private class ButtonHandler implements ActionListener{	
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(signInBtn_)) {
					if (SignIn()) Core.MainGUIPopup(frame_);
				}
				if (e.getSource().equals(signUpBtn_)) {
					Core.CreateUserPopup(frame_);
				}
			}
		}
	
	public boolean SignIn() {
		try {
			if (Core.Login(userField_.getText(), passwordField_.getPassword())) 
				return true;
		} catch (SQLException e) {
			//Do nothing...
		}
		JOptionPane.showMessageDialog(null, "The user could not be found, or the user does not exist.\n"
				+ "Please try again, or create a new user.");
		return false;
	}
	
	public JFrame getFrame() {
		return frame_;
	}
}
