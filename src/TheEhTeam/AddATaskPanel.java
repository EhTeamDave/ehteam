package TheEhTeam;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Button;
import java.awt.TextField;
import java.awt.TextArea;
import javax.swing.JComboBox;

public class AddATaskPanel extends JPanel
{

	/**
	 * Create the panel.
	 */
	public AddATaskPanel()
	{
		setLayout(null);
		
		JLabel lblTaskTitle = new JLabel("Add A New Task");
		lblTaskTitle.setBounds(195, 13, 116, 16);
		add(lblTaskTitle);
		
		JLabel lblTaskProjectName = new JLabel("Project Name");
		lblTaskProjectName.setBounds(23, 61, 105, 16);
		add(lblTaskProjectName);
		
		JLabel lblTaskName = new JLabel("Task Name");
		lblTaskName.setBounds(23, 102, 92, 16);
		add(lblTaskName);
		
		JLabel lblTaskDescription = new JLabel("Task Description");
		lblTaskDescription.setBounds(23, 157, 105, 16);
		add(lblTaskDescription);
		
		Button btnAddTask = new Button("Add Task");
		btnAddTask.setBounds(137, 289, 79, 24);
		add(btnAddTask);
		
		Button btnTaskCancel = new Button("Cancel");
		btnTaskCancel.setBounds(240, 289, 79, 24);
		add(btnTaskCancel);
		
		TextField txtFldTaskName = new TextField();
		txtFldTaskName.setBounds(166, 94, 105, 24);
		add(txtFldTaskName);
		
		TextArea txtAreaTaskDescription = new TextArea();
		txtAreaTaskDescription.setBounds(166, 139, 284, 126);
		add(txtAreaTaskDescription);
		
		JComboBox cmbTaskProjectName = new JComboBox();
		cmbTaskProjectName.setBounds(166, 58, 105, 22);
		add(cmbTaskProjectName);

	}
}
