package TheEhTeam;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.Button;

public class AddAProjectPanel extends JPanel
{
	private JTextField txtFldProjectName;
	private JTextField txtFldNumberOfSprints;
	private JTextField txtFldDueDate;

	/**
	 * Create the panel.
	 */
	public AddAProjectPanel()
	{
		setLayout(null);
		
		JLabel lblProjectTitle = new JLabel("Add a New Project");
		lblProjectTitle.setBounds(143, 13, 116, 16);
		add(lblProjectTitle);
		
		JLabel lblProjectName = new JLabel("Project Name");
		lblProjectName.setBounds(32, 92, 79, 16);
		add(lblProjectName);
		
		JLabel lblNumSprints = new JLabel("Number of Sprints");
		lblNumSprints.setBounds(32, 131, 116, 16);
		add(lblNumSprints);
		
		JLabel lblDueDate = new JLabel("Due Date");
		lblDueDate.setBounds(32, 178, 56, 16);
		add(lblDueDate);
		
		JLabel lblProjectManager = new JLabel("Project Manager");
		lblProjectManager.setBounds(32, 219, 116, 16);
		add(lblProjectManager);
		
		txtFldProjectName = new JTextField();
		txtFldProjectName.setBounds(186, 89, 116, 22);
		add(txtFldProjectName);
		txtFldProjectName.setColumns(10);
		
		txtFldNumberOfSprints = new JTextField();
		txtFldNumberOfSprints.setBounds(186, 128, 116, 22);
		add(txtFldNumberOfSprints);
		txtFldNumberOfSprints.setColumns(10);
		
		txtFldDueDate = new JTextField();
		txtFldDueDate.setBounds(186, 175, 116, 22);
		add(txtFldDueDate);
		txtFldDueDate.setColumns(10);
		
		JComboBox cmbProjectManager = new JComboBox();
		cmbProjectManager.setBounds(186, 216, 93, 22);
		add(cmbProjectManager);
		
		Button btnAddProject = new Button("Add Project");
		btnAddProject.setBounds(117, 266, 79, 24);
		add(btnAddProject);
		
		Button btnProjectCancel = new Button("Cancel");
		btnProjectCancel.setBounds(226, 266, 79, 24);
		add(btnProjectCancel);

	}

}
