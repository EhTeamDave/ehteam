package DBTEST;
/**
 * Name: ConnectionManager.java
 * Purpose: A Tool Kit Designed to Optimize the Flow of Connecting to a MySQL Database, while providing additional functionality.
 * 			Coded for use with the mysql-connector-java-8.0.16.jar file. Make sure to include it too.
 * Coder: Samuel Penn (http://www.dcoding.tech)
 * Notes: This is Compiled from the JDK 1.8_77
 * Version History: 1.0 - Release (Written for 1.8) [2019-07-30]
 * 					1.1 - Updated dumpResultSet() to dumpResultSetToConsole() and added dumpResultSetToString() [2020-02-21]
 * 					1.2 - Handle Nullification of Login Variables through an configurable if statement [2020-02-21]
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class ConnectionManager {

	//--Self Reference
	//private static ConnectionManager connectionManager_;
	
	//--Login Variables
	private String host_;
	private String userName_;
	private String pwd_;
	
	//--Connector Variables
	private Connection connection_ 			= null;
	private Statement statement_ 			= null;
	private ResultSet resultSet_ 			= null;
	
	//--Debug Master Controller
	private Boolean debugger_ 				= false;		//Debug
	private Boolean printErrorsByJOption_ 	= false;		//Instead of printing errors by Text, print Errors by JOptionPane
	
	//--Configuration Variables
	private Boolean resetConnectionOnClose_ = false;
	
 	public ConnectionManager(){
		host_ = "jdbc:mysql://localhost:3306/hcm?useSSL=false";
		userName_ = "root";
		pwd_ = "root";
	}
 	
 	/*public ConnectionManager(ConnectionManager connectionManager) {
		connectionManager_ = connectionManager;
	}*/
 	
 	public ConnectionManager(String user, String pwd){
		host_ = "";
		userName_ = user;
		pwd_ = pwd;
	}
	
 	public ConnectionManager(String host, String user, String pwd){
		host_ = host;
		userName_ = user;
		pwd_ = pwd;
	}
	
 	public ConnectionManager(String host, String user, char [] pwd){
		host_ = host;
		userName_ = user;
		pwd_ = pwd.toString();
	}
	
	//--Methods
	
	/*
	 * Name: initiateConnection()
	 * Returns: True - if Successfull, false if Unsuccessful
	 * Notes: Safely Handles Initiation
	 */
	public boolean initiateConnection() {
		try {
			connection_ = DriverManager.getConnection(host_, userName_, pwd_);
			statement_ = connection_.createStatement();
			
		} catch (Exception e) {
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();
			}
			return false;
		}
		return true;
	}
	
	/*
	 * Name: initiateConnection()
	 * Takes: String, String
	 * Returns: True - if Successfull, false if Unsuccessful
	 * Notes: Safely Handles Initiation, overrides global variables
	 */
	public boolean initiateConnection(String userName, String pwd) {
		try {
			connection_ = DriverManager.getConnection(host_, userName, pwd);
			statement_ = connection_.createStatement();
			
		} catch (Exception e) {
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();
			}
			return false;
		}
		return true;
	}
	
	/*
	 * Name: initiateConnection()
	 * Takes: String, String, String
	 * Returns: True - if Successfull, false if Unsuccessful
	 * Notes: Safely Handles Initiation, overrides global variables
	 */
	public boolean initiateConnection(String host, String userName, String pwd) {
		try {
			connection_ = DriverManager.getConnection(host, userName, pwd);
			statement_ = connection_.createStatement();
			
		} catch (Exception e) {
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();
			}
			return false;
		}
		return true;
	}

	/*
	 * Name: closeConnection()
	 * Returns: True - if Successfull, false if Unsuccessful
	 * Notes: Safely Handles Closure. This will nullify the 'Login Variables' for safety reasons
	 * Revision: Handle Nulification of Login Variables through an configurable if statement [2020-02-21]
	 */
	public boolean closeConnection() {
		try {
			if (resultSet_ != null)
				resultSet_.close();
			if (statement_ != null)
				statement_.close();
			if (connection_ != null)
				connection_.close();
		} catch (Exception e) {
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();
			}
		} finally {
			try {
				if (resultSet_ != null) resultSet_.close();	
				if (statement_ != null) statement_.close();	
				if (connection_ != null) connection_.close();
			} catch (Exception e) {
				return false;
			} finally {
				if (resetConnectionOnClose_) {
					host_ 		= null;
					userName_ 	= null;
					pwd_ 		= null;
				}
			}
		}
		return true;
	}
	
	/*
	 * Name: executeUpdate
	 * Takes: String
	 * Returns: int
	 * Notes: returns the number of rows updated
	 */
	public int executeUpdate(String updateString) {
		final int ERROR = -1;
		int updatedRows = 0;
		
		try {
			updatedRows = statement_.executeUpdate(updateString);
		} catch (Exception e){
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();	
			}
			return ERROR;
		} 
		return updatedRows;
	}
	
	/*
	 * Name: executeQuery
	 * Takes: String
	 * Returns: ResultSet
	 * Notes: Can be called from inside any 'dump...' to make it a one-liner 
	 */
	public ResultSet executeQuery(String queryString) {
		try {
			resultSet_ = statement_.executeQuery(queryString);
			
		} catch (Exception e) {
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();	
			}
			return null;
		}
		return resultSet_;
	}
	
	/*
	 * Name: dumpResultSetToConsole
	 * Takes: ResultSet
	 * Notes: Thanks to Zeb from StackOverflow for dynamic solution. (https://stackoverflow.com/questions/24229442/print-the-data-in-resultset-along-with-column-names)
	 * Revision: Name Change [2020-02-21]
	 */
	public void dumpResultSetToConsole(ResultSet rs) {
		ResultSetMetaData rsmd = null;
		
		try {
			rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			
			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					if (i > 1) System.out.print(",  ");
					String columnValue = rs.getString(i);
					System.out.print(columnValue + " " + rsmd.getColumnName(i));
				}
				System.out.println("");
			}
		} catch (SQLException e) {
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();
			}
		} 
	}
	
	/*
	 * Name: dumpResultSetToString
	 * Takes: ResultSet
	 * Notes: Thanks to Zeb from StackOverflow for dynamic solution. (https://stackoverflow.com/questions/24229442/print-the-data-in-resultset-along-with-column-names)
	 */
	public String dumpResultSetToString(ResultSet rs) {
		String returnString = "";
		ResultSetMetaData rsmd = null;
		
		try {
			rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			
			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					if (i > 1) returnString += (", ");
					String columnValue = rs.getString(i);
					returnString += (columnValue + " " + rsmd.getColumnName(i));
				}
				returnString += "/n";
			}
		} catch (SQLException e) {
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();
			}
		} 
		return returnString;
	}
	
	/*
	 * Name: dumpResultSetToTableModel
	 * Takes: ResultSet
	 * Returns: TableModel
	 * Notes: Thanks to Charles, from Technojeeves.com/joomla/index.pho/free/59-resultset-to-tablemodel
	 */
	public TableModel dumpResultSetToTableModel(ResultSet rs){
		ResultSetMetaData rsmd = null;
		
        try {
        	rsmd = rs.getMetaData();
            int numberOfColumns = rsmd.getColumnCount();
            Vector<String> columnNames = new Vector<String>();

            for (int column = 0; column < numberOfColumns; column++) {
                columnNames.addElement(rsmd.getColumnLabel(column + 1));
                //NOTE: need to do the (+1) because metaData columns indexing starts at 1
                //but JTable column numbering starts at 0....G-r-r-r-r! 
            }

            Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

            while (rs.next()) {
            	Vector<Object> newRow = new Vector<Object>();
	
            	for (int i = 1; i <= numberOfColumns; i++) 
            		newRow.addElement(rs.getObject(i));
	
	            rows.addElement(newRow);
            }
            
            return new DefaultTableModel(rows, columnNames);
        } catch (Exception e){
			if (debugger_) {
				if (printErrorsByJOption_) JOptionPane.showMessageDialog(null, e.getMessage());
				else e.printStackTrace();	
			}
        	return null;
        }
	}

	//--Getters/Setters
	public String getHost_() {
		return host_;
	}

	public void setHost_(String host_) {
		this.host_ = host_;
	}

	public String getUserName_() {
		return userName_;
	}

	public void setUserName_(String userName_) {
		this.userName_ = userName_;
	}

	public String getPwd_() {
		return pwd_;
	}

	public void setPwd_(String pwd_) {
		this.pwd_ = pwd_;
	}

	public Connection getConnection_() {
		return connection_;
	}

	public void setConnection_(Connection connection_) {
		this.connection_ = connection_;
	}

	public Statement getStatement_() {
		return statement_;
	}

	public void setStatement_(Statement statement_) {
		this.statement_ = statement_;
	}

	public ResultSet getResultSet_() {
		return resultSet_;
	}

	public void setResultSet_(ResultSet resultSet_) {
		this.resultSet_ = resultSet_;
	}

	public Boolean getDebugger_() {
		return debugger_;
	}

	public void setDebugger_(Boolean debugger_) {
		this.debugger_ = debugger_;
	}

	public Boolean getPrintErrorsByJOption_() {
		return printErrorsByJOption_;
	}

	public void setPrintErrorsByJOption_(Boolean printErrorsByJOption_) {
		this.printErrorsByJOption_ = printErrorsByJOption_;
	}

	public Boolean getResetConnectionOnClose_() {
		return resetConnectionOnClose_;
	}

	public void setResetConnectionOnClose_(Boolean resetConnectionOnClose_) {
		this.resetConnectionOnClose_ = resetConnectionOnClose_;
	}	

	/*
	public static void main(String [] args) {
		ConnectionManager cM = new ConnectionManager();
		cM.initiateConnection();
		cM.dumpResultSetToConsole(cM.executeQuery("SELECT * FROM employees"));
		cM.closeConnection();
	}
	*/
}